'use strict';

const util = require('util');

if (process.argv.length !== 4) {
  console.error(`usage: ${process.argv[1]} <access-token> <offset>`);
  process.exit(1);
}

const accessToken = process.argv[2];
const offset      = parseInt(process.argv[3], 10);

// Hit the Exhibitor API supplying the offset query param.
const request = require('request');
const options = {
  uri: 'https://dev5-api.americasmart.com/v1.2/Exhibitor',
  headers: {
    Authorization: `Bearer ${accessToken}`,
    'Content-Type': 'application/json'
  },
  method: 'GET',
  qs: { offset }
};

request(options, function(err, resp, body) {
  if (resp.statusCode < 200 || resp.statusCode >= 300) {
    console.error('Error response from token endpoint.');
    console.error(body);
  }
  else {
    const companies = JSON.parse(body);

    console.log('Received the following response:');
    console.log(util.inspect(companies, {depth: null, compact: false}));
  }
});

